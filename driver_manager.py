from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import presence_of_element_located

class DriverManager:
    __instance = object

    _driver_instance = None

    #singletone pattern implementation
    #in both cases with single ang multithreaded run
    #only one class instance is needed to provide appropirate driver instance into BasePage
    @classmethod
    def getInstance(self):
        if not isinstance(DriverManager.__instance, DriverManager):
            DriverManager.__instance = DriverManager()
        return DriverManager.__instance

    #Provides driver instance
    #ToDo: for multithreaded run should be extended into providing driver instance from driver pool
    def get_driver(self):
        if not self._driver_instance:
            self._driver_instance = webdriver.Chrome('C:/Users/Nikolay/Downloads/chromedriver_win32/chromedriver.exe')
        return self._driver_instance
    
    #call from test teardown
    #closes current browser
    #todo: for multithreaded run should be extended
    def close_driver(self):
        self._driver_instance
        if self._driver_instance:
            self._driver_instance.quit()
            self._driver_instance = None
