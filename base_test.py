from base_page import BasePage
from driver_manager import DriverManager

class BaseTest:
    def setup(self):
        print('setup method passed')

    def teardown(self):
        manager = DriverManager.getInstance()
        manager.close_driver()