from driver_manager import DriverManager

class BasePage:
    def __init__(self):
        manager = DriverManager.getInstance()
        self.driver = manager.get_driver()